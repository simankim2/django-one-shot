from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem


def todo_list_list(request):
    todolist = TodoList.objects.all()
    context = {"todo_list_list": todolist}
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):
    todoitem = TodoItem.objects.get(id=id)
    context = {"todo_list_item": todoitem}
    return render(request, "todos/todo_details.html", context)
